# Reproducible Research: Peer Assessment 1
In this peer assestment are solved five questions. Each of them are explained in this 
report with its corresponding code.

## Loading and preprocessing the data
Here we load the database. It is located in the folder PA 1 in my computer. After that, 
we modify the character variable that cotains the dates and converted into a Date object.

```r
data <- read.csv(file = '/home/rocampo/Workspace/Reproducible Research/PA 1/activity.csv')
data["date"] <- as.Date(x = data$date, format = "%Y-%m-%d")
```


## What is mean total number of steps taken per day?

The first question is to find the mean total number of steps taken per day, therefore, 
I use the tapply function in order to sum all the steps walked in each day. After that,
a histogram is created.

```r
stepsByDay <- tapply( X = data$steps, 
                     INDEX = as.character(data$date), 
                     FUN = sum, na.rm = T )
hist(x = stepsByDay, main = "Histogram of Number of Steps Tanken Each Day",
     xlab = "Steps")
```

![](PA1_template_files/figure-html/unnamed-chunk-2-1.png) 

The mean of the distribution is approximately 9354 and the 
median is 10395.

## What is the average daily activity pattern?
The lectures are taken each five minutes. In this graphic we try to show the activity
of the person during the day in average. In this case, we use tapply and calculate the
mean steaps for each interval. A line plot is located following.

```r
avgByInterval <- tapply( X = data$steps, 
                        INDEX = data$interval, 
                        FUN = mean, na.rm = T)
plot( avgByInterval , type = 'l', xlab = "Interval", ylab = "Mean Steps")
```

![](PA1_template_files/figure-html/unnamed-chunk-3-1.png) 

The maximum value is presented in the position 835 of the vector in the interval 104,
with an approximate value of 200.

```r
which.max( avgByInterval )
```

```
## 835 
## 104
```

## Imputing missing values
The number of missing values is 2304. In order to impute the 
missing values I use the mean of the day intervals. In the first line I get the 
positions in the vector where NA's occur. After that I copy the original data 
frame in a new one named data.na. Finally, I substitute all the NA with the 
mean corresponding to the interval of the row.



```r
na.obs <- data$interval[ which( is.na( data$steps ) ) ]
data.na <- data
data["steps"][ is.na( data$steps ), ] <- avgByInterval[ as.character( na.obs ) ]
```

In order to show the results, it is plotted the same histogram as before but 
with the complete cases. In contrast with the last one, this presents more cases
between steps 5,000 and 15,000. The other ones are the same.

```r
stepsByDay <- tapply( X = data$steps, 
                     INDEX = as.character(data$date), 
                     FUN = sum, na.rm = T )
hist(x = stepsByDay, main = "Histogram of Number of Steps Tanken Each Day",
     xlab = "Steps")
```

![](PA1_template_files/figure-html/unnamed-chunk-6-1.png) 

With the imputed database the mean is 1.0766\times 10^{4} and the median 
1.0766\times 10^{4}. In this case is presented a possible simmetry in
contrast with the original database.


## Are there differences in activity patterns between weekdays and weekends?
Finally we want to know if there is a different pattern between weekdays and weekends.
Therefore, the database is split. A plot is showed. We can see taht in weekdays we have 
a maximum, but in weekends it shows a uniform increase in number of steps.

```r
data["var1"] <- ifelse( test = weekdays(x = data$date ) %in% c("Saturday", "Sunday"), 
                       yes = "Weekend", 
                       no = "Weekday" ) 
obs <- tapply( X = data$steps, INDEX = list( data$interval, data$var1 ) , FUN = mean )
library( reshape )
library( ggplot2 )
obs <- melt( obs )
plt <- ggplot( data = obs, aes( x = X1, y = value ) )
plt <- plt + geom_line() + xlab("Interval") + ylab("Number of steps")
plt + facet_grid( X2 ~ . )
```

![](PA1_template_files/figure-html/unnamed-chunk-7-1.png) 

